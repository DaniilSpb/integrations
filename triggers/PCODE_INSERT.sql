SET TERM ^ ;
CREATE TRIGGER PCODE_INSERT FOR CLIENTS ACTIVE
BEFORE insert POSITION 0
AS
BEGIN
  if ((new.pcode is null) or (new.pcode = 0)) then
  begin
   new.pcode = gen_id(gen_pcode, 1);
  end 
END^
SET TERM ; ^