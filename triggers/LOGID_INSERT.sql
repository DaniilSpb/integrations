SET TERM ^ ;
CREATE TRIGGER LOGID_INSERT FOR EXCHANGELOG ACTIVE
BEFORE insert POSITION 0
AS
BEGIN
  if ((new.logid is null) or (new.logid = 0)) then
  begin
   new.logid = gen_id(gen_logid, 1);
  end 
END^
SET TERM ; ^