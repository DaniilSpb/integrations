SET TERM ^ ;
CREATE TRIGGER HISTID_INSERT FOR CLHISTNUM ACTIVE
BEFORE insert POSITION 0
AS
BEGIN
  if ((new.histid is null) or (new.histid = 0)) then
  begin
   new.histid = gen_id(gen_histid, 1);
  end 
END^
SET TERM ; ^