CREATE TABLE CLIENTS
(
  PCODE integer NOT NULL,
  FIRSTNAME varchar(20),
  LASTNAME varchar(30),
  MIDNAME varchar(30),
  bdate date,
  pasper varchar (4),
  paspnum varchar(6),
  paspdate date,
  snils varchar(10),
  CONSTRAINT pcode_primary_key PRIMARY KEY (PCODE)
);
