CREATE TABLE STATS
(
  id integer,
  PCODE integer,
  DATE_request TIMESTAMP,
  info_wrongs varchar(256),
  CONSTRAINT id_PRIMARY_KEY PRIMARY KEY (id)
);