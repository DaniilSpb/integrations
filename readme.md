## Интеграция с внешним сервисом

В рамках данного задания необходимо будет реализовать логику обмена данными с внешней системой с помощью хранимых процедур Firebird,
а точнее логику формирования исходящих сообщений и логику обработки ответов от внешнего сервиса.

При этом предполагается, что сам непосредственный обмен будет осуществляться уже реализованной службой, 
которая 

- проверяет таблицу обмена `EXCHANGELOG` на предмет наличия необработанных исходящих сообщений, 
- формирует и отправляет http-запрос к внешней системе
- получает http-ответ от внешней системы и сохраняет его в таблицу `EXCHANGELOG` с тем же `msgid`
- вызывает настроенную для обмена процедуру обработки ответа, в которую передает ссылка на запись `EXCHANGELOG` и тело сообщения-ответа


В качестве результата по решению данной задачи предоставить:

- Скрипты создания необходимых для реализации обмена таблиц и хранимых процедур


#### Подготовка БД

- Создать таблицу `CLIENTS` с общими данными о пациентах, содержащая следующие поля:
    - `pcode` (целое, первичный ключ) - суррогатный идентификатор пациента, формируется генератором
    - `lastname` (строка) - фамилия пациента
    - `firstname` (строка) - имя пациента
    - `midname ` (строка) - отчество пациента
    - `bdate` (дата) - дата рождения пациента
    - `paspser` (строка) - серия паспорта РФ
    - `paspnum` (строка) - номер паспорта РФ
    - `paspdate` (дата) - дата выдачи паспарта РФ
    - `paspplace` (строка) - место выдачи паспорта РФ
    - `snils` (строка) - СНИЛС пациента
- Создать таблицу `CLHISTNUM` с данными о полисах мед. страхования пациента, содержащая следующие поля:
    - `histid` (целое, первичный ключ) - суррогатный идентификатор полиса, формируется генератором
    - `pcode` (целое) - ссылка на пациента, которому принадлежит полис
    - `nspser` (строка) - серия полиса
    - `nspnum` (строка) - номер полиса
    - `nspplace` (строка) - организация, выдавшая полис
- Создать таблицу `EXCHANGELOG` с данными о сообщениях в рамках обмена с внешней системой, содержащая следующие поля:
    - `logid` - (целое, первичный ключ) - суррогатный идентификатор сообщения обмена, формируется генератором
    - `logtype` (целое) - тип сообщения
        - `0` - исходящее
        - `1` - входящее
    - `logstate ` (целое) - статус сообщения
        - `1` - ждёт обработки
        - `2` - успешно обработано
        - `3` - обработано с ошибкой
    - `msgid` (строка) - идентификатор сессии обмена (должен быть одинаковым для пары сообщений "запрос-ответ")
    - `msgtext` (строка) - тело сообщения
- Создать генератор `CL_GEN` для идентификатора пациента и первичного ключа таблицы `CLIENTS` (`CLIENTS.pcode`)
- Создать генератор `HISTID_GEN` для первичного ключа записей в таблице с полисами мед. страхования (`CLHISTNUM.histid`)
- Создать генератор `EXCHANGELOG_GEN` для первичного ключа записей в журнале обмена (`EXCHANGELOG.logid`)


Запросы для добавления данных в БД

```sql
-- Добавление пациентов
insert into CLIENTS ( lastname, firstname, midname, bdate, paspser, paspnum, paspdate, paspplace, snils)
    values ('Первый', 'Пациент', 'Батькович', '01.02.2000', '1020', '443322', '01.02.2014', 'ОУФМС России №1', '175 073 380 71');
insert into CLIENTS ( lastname, firstname, midname, bdate, paspser, paspnum, paspdate, paspplace, snils)
    values ('Второй', 'Пациент', null, '01.02.1980', '4520', '883456', '05.11.1990', 'ОУФМС России №2', '335 594 380 00');

-- Добавление полисов мед. страхования для пациентов
insert into CLHISTNUM ( nspser, nspnum, nspplace, pcode)
    values ( null, '1234567890123456', 'Страховая №1', (select first 1 pcode from CLIENTS where lastname = 'Первый'));
insert into CLHISTNUM ( nspser, nspnum, nspplace, pcode)
    values ( 'ВС', '12345678', 'Страховая №2', (select first 1 pcode from CLIENTS where lastname = 'Второй'));
insert into CLHISTNUM ( nspser, nspnum, nspplace, pcode)
    values ( 'АМ', '1234421323453', 'Страховая №3', (select first 1 pcode from CLIENTS where lastname = 'Первый'));
```


#### Задание

Реализовать отправку данных по пациентам из МИС во внешнюю систему по описанному ниже протоколу информационного взаимодействия (ПИВ).

Отправку данных производить только в случае, когда данные о пациенте еще не отправлялись во внешнюю систему или же были изменены после последней отправки.

Предусмотреть сохранение в БД МИС данных об идентификаторе пациента во внешней системе (присвоенный ему в ходе успешной отправки данных).

Для возможности информирования пользователя МИС о результатах обмена путем формирования отчетов необходимо добавить в БД дополнительных таблицы,
в которых будет храниться доп. информация об обмене, такая как:

- дата/время обмена
- внешний идентификатор пациента во внешней системе
- информация об ошибках


#### Протокол информационного взаимодействия (ПИВ)

Формат обмена: `XML`.

Структура запроса (исходящего сообщения) для отправки данных по пациенту

| № п/п | Имя узла                      |      Тип      | Кратность | Описание |
|:------|:------------------------------|:-------------:|:----:|:--------|
| 1     | Envelope                      | Envelope      | 1..1 | SOAP-конверт |
| 1.1   | Envelope/Header               | Header        | 1..1 | Заголовок SOAP-конверта (в данном обмене - пустой узел) |
| 1.2   | Envelope/Body                 | Body          | 1..1 | Тело SOAP-конверта |
| 1.2.1 | Envelope/Body/Patient         | PatientData   | 1..1 | Данные по пациенту (структура описана в отдельной таблице ниже) |


Структура ответа сервиса на отправку данных по пациенту

Структура запроса (исходящего сообщения) для отправки данных по пациенту, который ранее уже отправлялся

| № п/п | Имя узла                      |      Тип      | Кратность | Описание |
|:------|:------------------------------|:-------------:|:----:|:--------|
| 1     | Envelope                      | Envelope      | 1..1 | SOAP-конверт |
| 1.1   | Envelope/Header               | Header        | 1..1 | Заголовок SOAP-конверта (в данном обмене - пустой узел) |
| 1.2   | Envelope/Body                 | Body          | 1..1 | Тело SOAP-конверта |
| 1.2.1 | Envelope/Body/Result          | PatientResult | 1..1 | Данные о результате добавления/обновления пациента |
| 1.2.2 | Envelope/Body/Result/success  | boolean       | 1..1 | Признак успешности операции: true - успешно; false - ошибка |
| 1.2.3 | Envelope/Body/Result/id       | string        | 0..1 | Идентификатор, присвоенный пациенту во внешней системе |
| 1.2.4 | Envelope/Body/Result/errors   | string        | 0..1 | Текст с описанием ошибки (если имеется) |


Структура узла `PatientData`

| № п/п | Имя узла                      |      Тип      | Кратность | Описание |
|:------|:------------------------------|:-------------:|:----:|:--------|
| 1     | mis_id                        | string        | 1..1 | Идентификатор пациента в МИС |
| 2     | lastname                      | string        | 1..1 | Фамилия пациента |
| 3     | firstname                     | string        | 1..1 | Имя пациента |
| 4     | surname                       | string        | 0..1 | Отчество пациента (если имеется) |
| 5     | birthdate                     | date          | 1..1 | Дата рождения пациента в ISO-формате (YYYY-MM-DD)|
| 6     | documents                     | DocumentsList | 1..1 | Узел, содержащий в себе список документов пациента |
| 6.1   | documents/document            | Document      | 0..* | Узел, содержащий описание отдельного документа пациента |
| 6.1.1 | documents/document/type       | integer       | 1..1 | Тип документа: 0 - паспорт; 1 - СНИЛС; 2 - полис ОМС |
| 6.1.2 | documents/document/series     | string        | 0..1 | Серия документа (если имеется, только цифры и буквы, без пробелов) |
| 6.1.3 | documents/document/number     | string        | 1..1 | Номер документа (только цифры) |
| 6.1.4 | documents/document/issued_on  | date          | 0..1 | Дата выдачи документа в ISO-формате (YYYY-MM-DD) |
| 6.1.5 | documents/document/expired_on | date          | 0..1 | Дата окончания действия документа  в ISO-формате (YYYY-MM-DD) |
| 6.1.6 | documents/document/issuer     | string        | 0..1 | Организация, которая выдала документ |


Пример запроса отправки данных о пациенте

```xml
<Envelope>
    <Header />
    <Body>
        <Patient>
            <mis_id>12345678</mis_id>
            <lastname>Иванов</lastname>
            <firstname>Петр</firstname>
            <surname>Сидорович</surname>
            <birthdate>2001-02-03</birthdate>
            <documents>
                <document>
                    <type>0</type>
                    <series>2018</series>
                    <number>886644</number>
                    <issued_on>2015-03-04</issued_on>
                    <issuer>МВД</issuer>
                </document>
                <document>
                    <type>1</type>
                    <number>79780131752</number>
                    <issuer>ПФР</issuer>
                </document>
                <document>
                    <type>2</type>
                    <number>1234567890123456</number>
                    <issued_on>2020-04-05</issued_on>
                    <issuer>Страховая №1</issuer>
                </document>
            </documents>
        </Patient>
    </Body>
</Envelope>
```

Пример ответа внешней системы об успешной обработке запроса с данными по пациенту

```xml
<Envelope>
    <Header />
    <Body>
        <Result>
            <success>true</success>
            <id>ext_id_1</id>
        </Result>
    </Body>
</Envelope>
```

Пример ответа внешней системы об обшике обработки запроса с данными по пациенту

```xml
<Envelope>
    <Header />
    <Body>
        <Result>
            <success>false</success>
            <errors>В системе обнаружен пациент с тем же идентификатором МИС, но с другими ФИО и датой рождения</errors>
        </Result>
    </Body>
</Envelope>
```