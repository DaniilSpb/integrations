SET TERM ^ ;
ALTER PROCEDURE XMLFORMAT (
    PCODE integer )
RETURNS (
    ID integer,
    FIRSTNAME varchar(50),
    LASTNAME varchar(50),
    NSPNUM varchar(50),
    NSPPLACE varchar(50),
    COUNTER integer,
    XMLFILE varchar(2048) )
AS
DECLARE VARIABLE last_name varchar(50);
DECLARE VARIABLE first_name varchar(50);
DECLARE VARIABLE surname varchar(50);
DECLARE VARIABLE birthdate varchar(50);
DECLARE VARIABLE documents varchar(512);
DECLARE VARIABLE doc_passport varchar(256);
DECLARE VARIABLE doc_snils varchar(256);
DECLARE VARIABLE doc_strah varchar(256);
DECLARE VARIABLE doc_strah_temp varchar(256);


BEGIN
    counter   = 0;
    xmlfile   = 'Envelope><Header /><Body><Patient> <mis_id>' || :pcode || '</mis_id>';
    last_name = '<lastname>'  ||  (select lastname  from CLIENTS where  pcode = :pcode)  || '</lastname>';
    firstname = '<firstname>'   ||  (select firstname from CLIENTS where  pcode = :pcode ) || '</firstname>';
    surname = '<surname>'   ||  (select coalesce(midname,'') from CLIENTS where  pcode = :pcode ) || '</surname>';
    birthdate = '<birthdate>' ||  (select bdate from CLIENTS where  pcode = :pcode ) || '</birthdate>';
    doc_passport = '<documents><document><type>0</type><series>' || (select paspser from CLIENTS where pcode =:pcode)|| 
    '</series>' || '<number>' || (select paspnum from clients where pcode = :pcode ) 
     || '</number>'  ||'<issued_on>' || (select paspdate from clients where pcode = :pcode ) 
     || '</issued_on>' ||'<issuer>' || '???' || '</issuer></document>';
    doc_snils = '<document><type>1</type><number>' || (select snils from clients where pcode = :pcode) 
                || '</number><issuer>???</issuer></document>';
    xmlfile = xmlfile || last_name || firstname || surname || birthdate || doc_passport || doc_snils;
    
    for SELECT 
        r.PCODE
    , r.FIRSTNAME
    , r.LASTNAME
    , chn.nspnum
    , chn.nspplace
    FROM CLIENTS r
        inner join CLHISTNUM chn on r.pcode = chn.pcode 
    where r.pcode = :pcode
        into id,firstname,lastname,nspnum,nspplace
    do begin
      counter = counter + 1;
      doc_strah_temp = '<document><type>2</type><number>' || nspnum || '</number><issued_on>2020-04-05</issued_on> <issuer>' || nspplace || '</issuer></document>';
      xmlfile = xmlfile || doc_strah_temp;

      suspend;
    end
    xmlfile = xmlfile || '</documents></Patient></Body></Envelope>';
    suspend;
    insert into  EXCHANGELOG(logtype,logstate,msgid,msgtext) VALUES (0,1,'123456789',:xmlfile);
    

    
END^
SET TERM ; ^